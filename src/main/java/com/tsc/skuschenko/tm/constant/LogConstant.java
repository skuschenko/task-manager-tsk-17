package com.tsc.skuschenko.tm.constant;

public interface LogConstant {

    String COMMANDS = "COMMANDS";

    String ERRORS = "ERRORS";

    String MESSAGES = "MESSAGES";

    String COMMANDS_FILE = "./commands.txt";

    String ERRORS_FILE = "./errors.txt";

    String MESSAGES_FILE = "./messages.txt";

    String LOG_PROPERTIES = "/logger.properties";

}
