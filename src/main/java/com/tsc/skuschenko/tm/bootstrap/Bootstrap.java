package com.tsc.skuschenko.tm.bootstrap;

import com.tsc.skuschenko.tm.api.repository.ICommandRepository;
import com.tsc.skuschenko.tm.api.repository.IProjectRepository;
import com.tsc.skuschenko.tm.api.repository.ITaskRepository;
import com.tsc.skuschenko.tm.api.service.*;
import com.tsc.skuschenko.tm.command.AbstractCommand;
import com.tsc.skuschenko.tm.command.project.*;
import com.tsc.skuschenko.tm.command.system.*;
import com.tsc.skuschenko.tm.command.task.*;
import com.tsc.skuschenko.tm.constant.InformationConst;
import com.tsc.skuschenko.tm.exception.system.UnknownArgumentException;
import com.tsc.skuschenko.tm.exception.system.UnknownCommandException;
import com.tsc.skuschenko.tm.repository.CommandRepository;
import com.tsc.skuschenko.tm.repository.ProjectRepository;
import com.tsc.skuschenko.tm.repository.TaskRepository;
import com.tsc.skuschenko.tm.service.*;
import com.tsc.skuschenko.tm.util.TerminalUtil;

public class Bootstrap implements IServiceLocator {

    private final ICommandRepository commandRepository =
            new CommandRepository();

    private final ICommandService commandService =
            new CommandService(commandRepository);

    private final ITaskRepository taskRepository =
            new TaskRepository();

    private final IProjectRepository projectRepository =
            new ProjectRepository();

    private final ITaskService taskService =
            new TaskService(taskRepository);

    private final IProjectService projectService =
            new ProjectService(projectRepository);

    private final IProjectTaskService projectTaskService =
            new ProjectTaskService(taskRepository, projectRepository);

    private final ILogService logService = new LogService();

    {
        registry(new ProjectViewByIdCommand());
        registry(new ProjectViewByIndexCommand());
        registry(new ProjectViewByNameCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectRemoveByNameCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectListCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectStartByNameCommand());
        registry(new ProjectFinishByIdCommand());
        registry(new ProjectFinishByIndexCommand());
        registry(new ProjectFinishByNameCommand());
        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectChangeStatusByNameCommand());
        registry(new ProjectDeleteByIdCommand());
        registry(new TaskCreateCommand());
        registry(new TaskClearCommand());
        registry(new TaskListCommand());
        registry(new TaskViewByIdCommand());
        registry(new TaskViewByIndexCommand());
        registry(new TaskViewByNameCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskRemoveByNameCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskStartByNameCommand());
        registry(new TaskFinishByIdCommand());
        registry(new TaskFinishByIndexCommand());
        registry(new TaskFinishByNameCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskChangeStatusByNameCommand());
        registry(new TaskFindAllByProjectIdCommand());
        registry(new TaskBindByProjectCommand());
        registry(new TaskUnbindFromProjectCommand());
        registry(new AboutShowCommand());
        registry(new AllArgumentsShowCommand());
        registry(new AllCommandsShowCommand());
        registry(new HelpShowCommand());
        registry(new ProgramExitCommand());
        registry(new SystemInfoShowCommand());
        registry(new VersionShowCommand());
    }

    public void run(final String... args) {
        logService.debug("TEST");
        logService.info("***Welcome to task manager***");
        if (parseArgs(args)) exit();
        while (true) {
            try {
                System.out.print("Enter command:");
                final String command = TerminalUtil.nextLine();
                logService.command(command);
                parseCommand(command);
                final String operationOk = InformationConst.OPERATION_OK;
                System.out.println("[" + operationOk.toUpperCase() + "]");
            } catch (Exception e) {
                logService.error(e);
                final String operationFail = InformationConst.OPERATION_FAIL;
                System.out.println("[" + operationFail.toUpperCase() + "]");
            }
        }
    }

    public void parseArg(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        final AbstractCommand abstractCommand
                = commandService.getCommandByArg(arg);
        if (abstractCommand == null) throw new UnknownArgumentException(arg);
        abstractCommand.execute();
    }

    public void parseCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        final AbstractCommand abstractCommand
                = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new UnknownCommandException(command);
        abstractCommand.execute();
    }

    public boolean parseArgs(final String[] args) {
        if (args == null || args.length < 1) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

    private void registry(final AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public void exit() {
        System.exit(0);
    }

}
