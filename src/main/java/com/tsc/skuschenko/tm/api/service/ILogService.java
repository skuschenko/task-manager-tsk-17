package com.tsc.skuschenko.tm.api.service;

public interface ILogService {

    void info(String message);

    void command(String message);

    void error(Exception e);

    void debug(String message);

}
