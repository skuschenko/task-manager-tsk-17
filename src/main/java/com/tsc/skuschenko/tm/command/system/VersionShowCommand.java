package com.tsc.skuschenko.tm.command.system;

import com.tsc.skuschenko.tm.command.AbstractCommand;
import com.tsc.skuschenko.tm.constant.ArgumentConst;
import com.tsc.skuschenko.tm.constant.TerminalConst;

public class VersionShowCommand extends AbstractCommand {

    @Override
    public String arg() {
        return ArgumentConst.VERSION;
    }

    @Override
    public String name() {
        return TerminalConst.VERSION;
    }

    @Override
    public String description() {
        return "version";
    }

    @Override
    public void execute() {
        showOperationInfo(TerminalConst.VERSION);
        System.out.println("1.0.0");
    }

}