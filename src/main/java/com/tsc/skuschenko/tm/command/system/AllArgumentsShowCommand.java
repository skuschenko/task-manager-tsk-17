package com.tsc.skuschenko.tm.command.system;

import com.tsc.skuschenko.tm.command.AbstractCommand;
import com.tsc.skuschenko.tm.constant.TerminalConst;

import java.util.Collection;

public class AllArgumentsShowCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConst.ARGUMENTS;
    }

    @Override
    public String description() {
        return "arguments";
    }

    @Override
    public void execute() {
        showOperationInfo(TerminalConst.ARGUMENTS);
        final Collection<String> names =
                serviceLocator.getCommandService().getListArgumentName();
        for (final String name : names) {
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

}
