package com.tsc.skuschenko.tm.command.project;

import com.tsc.skuschenko.tm.api.service.IProjectService;
import com.tsc.skuschenko.tm.constant.InformationConst;
import com.tsc.skuschenko.tm.constant.TerminalConst;
import com.tsc.skuschenko.tm.exception.entity.ProjectNotFoundException;
import com.tsc.skuschenko.tm.model.Project;
import com.tsc.skuschenko.tm.util.TerminalUtil;

public class ProjectUpdateByIdCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConst.PROJECT_UPDATE_BY_ID;
    }

    @Override
    public String description() {
        return "update project by id";
    }

    @Override
    public void execute() {
        showOperationInfo(TerminalConst.PROJECT_UPDATE_BY_ID);
        showParameterInfo(InformationConst.ID);
        final String valueId = TerminalUtil.nextLine();
        final IProjectService projectService
                = serviceLocator.getProjectService();
        Project project = projectService.findOneById(valueId);
        if (project == null) throw new ProjectNotFoundException();
        showParameterInfo(InformationConst.NAME);
        final String valueName = TerminalUtil.nextLine();
        showParameterInfo(InformationConst.DESCRIPTION);
        final String valueDescription = TerminalUtil.nextLine();
        project = projectService.updateOneById(
                valueId, valueName, valueDescription
        );
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }

}
