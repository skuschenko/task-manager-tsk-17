package com.tsc.skuschenko.tm.command.task;

import com.tsc.skuschenko.tm.api.service.ITaskService;
import com.tsc.skuschenko.tm.constant.InformationConst;
import com.tsc.skuschenko.tm.constant.TerminalConst;
import com.tsc.skuschenko.tm.exception.entity.TaskNotFoundException;
import com.tsc.skuschenko.tm.model.Task;
import com.tsc.skuschenko.tm.util.TerminalUtil;

public class TaskChangeStatusByIdCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConst.TASK_CHANGE_STATUS_BY_ID;
    }

    @Override
    public String description() {
        return "change task by id";
    }

    @Override
    public void execute() {
        showOperationInfo(TerminalConst.TASK_CHANGE_STATUS_BY_ID);
        showParameterInfo(InformationConst.ID);
        final String valueId = TerminalUtil.nextLine();
        final ITaskService taskService = serviceLocator.getTaskService();
        Task task = taskService.findOneById(valueId);
        if (task == null) throw new TaskNotFoundException();
        task = taskService.changeTaskStatusById(
                valueId, readTaskStatus()
        );
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

}
