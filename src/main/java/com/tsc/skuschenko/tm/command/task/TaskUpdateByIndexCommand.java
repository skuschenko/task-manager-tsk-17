package com.tsc.skuschenko.tm.command.task;

import com.tsc.skuschenko.tm.api.service.ITaskService;
import com.tsc.skuschenko.tm.constant.InformationConst;
import com.tsc.skuschenko.tm.constant.TerminalConst;
import com.tsc.skuschenko.tm.exception.entity.TaskNotFoundException;
import com.tsc.skuschenko.tm.model.Task;
import com.tsc.skuschenko.tm.util.TerminalUtil;

public class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConst.TASK_UPDATE_BY_INDEX;
    }

    @Override
    public String description() {
        return "update task by index";
    }

    @Override
    public void execute() {
        showOperationInfo(TerminalConst.TASK_UPDATE_BY_INDEX);
        showParameterInfo(InformationConst.INDEX);
        final Integer valueIndex = TerminalUtil.nextNumber() - 1;
        final ITaskService taskService
                = serviceLocator.getTaskService();
        Task task = taskService.findOneByIndex(valueIndex);
        if (task == null) throw new TaskNotFoundException();
        showParameterInfo(InformationConst.NAME);
        final String valueName = TerminalUtil.nextLine();
        showParameterInfo(InformationConst.DESCRIPTION);
        final String valueDescription = TerminalUtil.nextLine();
        task = taskService.updateOneByIndex(
                valueIndex, valueName, valueDescription
        );
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

}