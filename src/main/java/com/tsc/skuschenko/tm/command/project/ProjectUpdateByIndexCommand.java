package com.tsc.skuschenko.tm.command.project;

import com.tsc.skuschenko.tm.api.service.IProjectService;
import com.tsc.skuschenko.tm.constant.InformationConst;
import com.tsc.skuschenko.tm.constant.TerminalConst;
import com.tsc.skuschenko.tm.exception.entity.ProjectNotFoundException;
import com.tsc.skuschenko.tm.model.Project;
import com.tsc.skuschenko.tm.util.TerminalUtil;

public class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConst.PROJECT_UPDATE_BY_INDEX;
    }

    @Override
    public String description() {
        return "update project by index";
    }

    @Override
    public void execute() {
        showOperationInfo(TerminalConst.PROJECT_UPDATE_BY_INDEX);
        showParameterInfo(InformationConst.INDEX);
        final Integer valueIndex = TerminalUtil.nextNumber() - 1;
        final IProjectService projectService
                = serviceLocator.getProjectService();
        Project project = projectService.findOneByIndex(valueIndex);
        if (project == null) throw new ProjectNotFoundException();
        showParameterInfo(InformationConst.NAME);
        final String valueName = TerminalUtil.nextLine();
        showParameterInfo(InformationConst.DESCRIPTION);
        final String valueDescription = TerminalUtil.nextLine();
        project = projectService.updateOneByIndex(
                valueIndex, valueName, valueDescription
        );
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }

}
