package com.tsc.skuschenko.tm.command.task;

import com.tsc.skuschenko.tm.api.service.IProjectTaskService;
import com.tsc.skuschenko.tm.constant.InformationConst;
import com.tsc.skuschenko.tm.constant.TerminalConst;
import com.tsc.skuschenko.tm.exception.entity.TaskNotFoundException;
import com.tsc.skuschenko.tm.model.Task;
import com.tsc.skuschenko.tm.util.TerminalUtil;

import java.util.List;

public class TaskFindAllByProjectIdCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConst.FIND_ALL_TASK_BY_PROJECT_ID;
    }

    @Override
    public String description() {
        return "find all task by project id";
    }

    @Override
    public void execute() {
        showOperationInfo(TerminalConst.FIND_ALL_TASK_BY_PROJECT_ID);
        showParameterInfo(InformationConst.PROJECT_ID);
        final String value = TerminalUtil.nextLine();
        final IProjectTaskService projectTaskService =
                serviceLocator.getProjectTaskService();
        final List<Task> tasks
                = projectTaskService.findAllTaskByProjectId(value);
        if (tasks == null || tasks.size() == 0) {
            throw new TaskNotFoundException();
        }
        tasks.forEach(item -> showTask(item));
    }

}
