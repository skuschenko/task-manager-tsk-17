package com.tsc.skuschenko.tm.command.project;

import com.tsc.skuschenko.tm.api.service.IProjectTaskService;
import com.tsc.skuschenko.tm.constant.TerminalConst;

public class ProjectClearCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConst.PROJECT_CLEAR;
    }

    @Override
    public String description() {
        return "clear all projects";
    }

    @Override
    public void execute() {
        showOperationInfo(TerminalConst.PROJECT_CLEAR);
        final IProjectTaskService projectTaskService =
                serviceLocator.getProjectTaskService();
        projectTaskService.clearProjects();
    }

}
