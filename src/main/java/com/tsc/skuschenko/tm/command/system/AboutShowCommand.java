package com.tsc.skuschenko.tm.command.system;

import com.tsc.skuschenko.tm.command.AbstractCommand;
import com.tsc.skuschenko.tm.constant.ArgumentConst;
import com.tsc.skuschenko.tm.constant.TerminalConst;

public class AboutShowCommand extends AbstractCommand {

    @Override
    public String arg() {
        return ArgumentConst.ABOUT;
    }

    @Override
    public String name() {
        return TerminalConst.ABOUT;
    }

    @Override
    public String description() {
        return "about";
    }

    @Override
    public void execute() {
        showOperationInfo(TerminalConst.ABOUT);
        System.out.println("AUTHOR: Semyon Kuschenko");
        System.out.println("EMAIL: skushchenko@tsconsulting.com");
    }

}
