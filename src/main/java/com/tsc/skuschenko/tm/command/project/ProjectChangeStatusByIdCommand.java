package com.tsc.skuschenko.tm.command.project;

import com.tsc.skuschenko.tm.api.service.IProjectService;
import com.tsc.skuschenko.tm.constant.InformationConst;
import com.tsc.skuschenko.tm.constant.TerminalConst;
import com.tsc.skuschenko.tm.exception.entity.ProjectNotFoundException;
import com.tsc.skuschenko.tm.model.Project;
import com.tsc.skuschenko.tm.util.TerminalUtil;

public class ProjectChangeStatusByIdCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConst.PROJECT_CHANGE_STATUS_BY_ID;
    }

    @Override
    public String description() {
        return "change project by id";
    }

    @Override
    public void execute() {
        showOperationInfo(TerminalConst.PROJECT_CHANGE_STATUS_BY_ID);
        showParameterInfo(InformationConst.ID);
        final String valueId = TerminalUtil.nextLine();
        final IProjectService projectService
                = serviceLocator.getProjectService();
        Project project = projectService.findOneById(valueId);
        if (project == null) throw new ProjectNotFoundException();
        project = projectService.changeProjectStatusById(
                valueId, readProjectStatus()
        );
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }

}
