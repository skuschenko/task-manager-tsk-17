package com.tsc.skuschenko.tm.command.project;

import com.tsc.skuschenko.tm.api.service.IProjectTaskService;
import com.tsc.skuschenko.tm.constant.InformationConst;
import com.tsc.skuschenko.tm.constant.TerminalConst;
import com.tsc.skuschenko.tm.exception.entity.ProjectNotFoundException;
import com.tsc.skuschenko.tm.model.Project;
import com.tsc.skuschenko.tm.util.TerminalUtil;

public class ProjectDeleteByIdCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConst.REMOVE_PROJECT_BY_ID;
    }

    @Override
    public String description() {
        return "remove project by id";
    }

    @Override
    public void execute() {
        showOperationInfo(TerminalConst.REMOVE_PROJECT_BY_ID);
        showParameterInfo(InformationConst.ID);
        final String value = TerminalUtil.nextLine();
        final IProjectTaskService projectTaskService =
                serviceLocator.getProjectTaskService();
        final Project project = projectTaskService.deleteProjectById(value);
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }

}
