package com.tsc.skuschenko.tm.command.system;

import com.tsc.skuschenko.tm.command.AbstractCommand;
import com.tsc.skuschenko.tm.constant.ArgumentConst;
import com.tsc.skuschenko.tm.constant.TerminalConst;

import java.util.Collection;

public class HelpShowCommand extends AbstractCommand {

    @Override
    public String arg() {
        return ArgumentConst.HELP;
    }

    @Override
    public String name() {
        return TerminalConst.HELP;
    }

    @Override
    public String description() {
        return "help";
    }

    @Override
    public void execute() {
        showOperationInfo(TerminalConst.HELP);
        final Collection<AbstractCommand> commands =
                serviceLocator.getCommandService().getCommands();
        for (final AbstractCommand command : commands) {
            if (command == null) continue;
            String result = "";
            if (command.name() != null && !command.name().isEmpty()) {
                result += command.name();
            }
            if (command.arg() != null && !command.arg().isEmpty()) {
                result += " [" + command.arg() + "]";
            }
            if (command.description() != null
                    && !command.description().isEmpty()) {
                result += " - " + command.description();
            }
            System.out.println(result);
        }
    }

}
