package com.tsc.skuschenko.tm.command.project;

import com.tsc.skuschenko.tm.api.service.IProjectService;
import com.tsc.skuschenko.tm.constant.InformationConst;
import com.tsc.skuschenko.tm.constant.TerminalConst;
import com.tsc.skuschenko.tm.exception.entity.ProjectNotFoundException;
import com.tsc.skuschenko.tm.model.Project;
import com.tsc.skuschenko.tm.util.TerminalUtil;

public class ProjectCreateCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConst.PROJECT_CREATE;
    }

    @Override
    public String description() {
        return "create new project";
    }

    @Override
    public void execute() {
        showOperationInfo(TerminalConst.PROJECT_CREATE);
        showParameterInfo(InformationConst.NAME);
        final String name = TerminalUtil.nextLine();
        showParameterInfo(InformationConst.DESCRIPTION);
        final String description = TerminalUtil.nextLine();
        final IProjectService projectService
                = serviceLocator.getProjectService();
        final Project project = projectService.add(name, description);
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }

}
