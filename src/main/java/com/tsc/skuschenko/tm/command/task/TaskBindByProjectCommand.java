package com.tsc.skuschenko.tm.command.task;

import com.tsc.skuschenko.tm.api.service.IProjectTaskService;
import com.tsc.skuschenko.tm.constant.InformationConst;
import com.tsc.skuschenko.tm.constant.TerminalConst;
import com.tsc.skuschenko.tm.exception.entity.TaskNotFoundException;
import com.tsc.skuschenko.tm.model.Task;
import com.tsc.skuschenko.tm.util.TerminalUtil;

public class TaskBindByProjectCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConst.BIND_TASK_BY_PROJECT;
    }

    @Override
    public String description() {
        return "bind task by project";
    }

    @Override
    public void execute() {
        showOperationInfo(TerminalConst.BIND_TASK_BY_PROJECT);
        showParameterInfo(InformationConst.PROJECT_ID);
        final String projectId = TerminalUtil.nextLine();
        showParameterInfo(InformationConst.TASK_ID);
        final String taskId = TerminalUtil.nextLine();
        final IProjectTaskService projectTaskService =
                serviceLocator.getProjectTaskService();
        Task task = projectTaskService.bindTaskByProject(projectId, taskId);
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

}