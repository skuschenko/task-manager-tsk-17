package com.tsc.skuschenko.tm.command.system;

import com.tsc.skuschenko.tm.command.AbstractCommand;
import com.tsc.skuschenko.tm.constant.TerminalConst;

import java.util.Collection;

public class AllCommandsShowCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConst.COMMANDS;
    }

    @Override
    public String description() {
        return "commands";
    }

    @Override
    public void execute() {
        showOperationInfo(TerminalConst.COMMANDS);
        final Collection<String> names =
                serviceLocator.getCommandService().getListCommandNames();
        for (final String name : names) {
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

}
