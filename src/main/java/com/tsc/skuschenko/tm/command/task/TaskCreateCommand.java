package com.tsc.skuschenko.tm.command.task;

import com.tsc.skuschenko.tm.api.service.ITaskService;
import com.tsc.skuschenko.tm.constant.InformationConst;
import com.tsc.skuschenko.tm.constant.TerminalConst;
import com.tsc.skuschenko.tm.exception.entity.TaskNotFoundException;
import com.tsc.skuschenko.tm.model.Task;
import com.tsc.skuschenko.tm.util.TerminalUtil;

public class TaskCreateCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConst.TASK_CREATE;
    }

    @Override
    public String description() {
        return "create new task";
    }

    @Override
    public void execute() {
        showOperationInfo(TerminalConst.TASK_CREATE);
        showParameterInfo(InformationConst.NAME);
        final String name = TerminalUtil.nextLine();
        showParameterInfo(InformationConst.DESCRIPTION);
        final String description = TerminalUtil.nextLine();
        final ITaskService taskService = serviceLocator.getTaskService();
        final Task task = taskService.add(name, description);
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

}
