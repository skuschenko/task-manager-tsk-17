package com.tsc.skuschenko.tm.command.task;

import com.tsc.skuschenko.tm.api.service.ITaskService;
import com.tsc.skuschenko.tm.constant.TerminalConst;

public class TaskClearCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConst.TASK_CLEAR;
    }

    @Override
    public String description() {
        return "clear all tasks";
    }

    @Override
    public void execute() {
        showOperationInfo(TerminalConst.TASK_CLEAR);
        final ITaskService taskService = serviceLocator.getTaskService();
        taskService.clear();
    }

}
