package com.tsc.skuschenko.tm.command.project;

import com.tsc.skuschenko.tm.api.service.IProjectService;
import com.tsc.skuschenko.tm.constant.InformationConst;
import com.tsc.skuschenko.tm.constant.TerminalConst;
import com.tsc.skuschenko.tm.exception.entity.ProjectNotFoundException;
import com.tsc.skuschenko.tm.model.Project;
import com.tsc.skuschenko.tm.util.TerminalUtil;

public class ProjectStartByNameCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConst.PROJECT_START_BY_NAME;
    }

    @Override
    public String description() {
        return "start project by name";
    }

    @Override
    public void execute() {
        showOperationInfo(TerminalConst.PROJECT_START_BY_NAME);
        showParameterInfo(InformationConst.NAME);
        final String value = TerminalUtil.nextLine();
        final IProjectService projectService
                = serviceLocator.getProjectService();
        final Project project = projectService.startProjectByName(value);
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }

}
