package com.tsc.skuschenko.tm.command.task;

import com.tsc.skuschenko.tm.api.service.ITaskService;
import com.tsc.skuschenko.tm.constant.InformationConst;
import com.tsc.skuschenko.tm.constant.TerminalConst;
import com.tsc.skuschenko.tm.exception.entity.TaskNotFoundException;
import com.tsc.skuschenko.tm.model.Task;
import com.tsc.skuschenko.tm.util.TerminalUtil;

public class TaskFinishByIdCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConst.TASK_FINISH_BY_ID;
    }

    @Override
    public String description() {
        return "finish task by id";
    }

    @Override
    public void execute() {
        showOperationInfo(TerminalConst.TASK_FINISH_BY_ID);
        showParameterInfo(InformationConst.ID);
        final String value = TerminalUtil.nextLine();
        final ITaskService taskService = serviceLocator.getTaskService();
        final Task task = taskService.completeTaskById(value);
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

}
