package com.tsc.skuschenko.tm.command.task;

import com.tsc.skuschenko.tm.api.service.ITaskService;
import com.tsc.skuschenko.tm.constant.InformationConst;
import com.tsc.skuschenko.tm.constant.TerminalConst;
import com.tsc.skuschenko.tm.exception.entity.TaskNotFoundException;
import com.tsc.skuschenko.tm.model.Task;
import com.tsc.skuschenko.tm.util.TerminalUtil;

public class TaskFinishByIndexCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConst.TASK_FINISH_BY_INDEX;
    }

    @Override
    public String description() {
        return "finish task by index";
    }

    @Override
    public void execute() {
        showOperationInfo(TerminalConst.TASK_FINISH_BY_INDEX);
        showParameterInfo(InformationConst.INDEX);
        final Integer value = TerminalUtil.nextNumber() - 1;
        final ITaskService taskService = serviceLocator.getTaskService();
        final Task task = taskService.completeTaskByIndex(value);
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

}
