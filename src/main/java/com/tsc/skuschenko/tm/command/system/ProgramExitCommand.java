package com.tsc.skuschenko.tm.command.system;

import com.tsc.skuschenko.tm.command.AbstractCommand;
import com.tsc.skuschenko.tm.constant.TerminalConst;

public class ProgramExitCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConst.EXIT;
    }

    @Override
    public String description() {
        return "exit";
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}
