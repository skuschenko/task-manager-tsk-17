package com.tsc.skuschenko.tm.command.task;

import com.tsc.skuschenko.tm.api.service.ITaskService;
import com.tsc.skuschenko.tm.constant.InformationConst;
import com.tsc.skuschenko.tm.constant.TerminalConst;
import com.tsc.skuschenko.tm.exception.entity.TaskNotFoundException;
import com.tsc.skuschenko.tm.model.Task;
import com.tsc.skuschenko.tm.util.TerminalUtil;

public class TaskChangeStatusByNameCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConst.TASK_CHANGE_STATUS_BY_NAME;
    }

    @Override
    public String description() {
        return "change task by name";
    }

    @Override
    public void execute() {
        showOperationInfo(TerminalConst.TASK_CHANGE_STATUS_BY_NAME);
        showParameterInfo(InformationConst.NAME);
        final String value = TerminalUtil.nextLine();
        final ITaskService taskService = serviceLocator.getTaskService();
        Task task = taskService.findOneByName(value);
        if (task == null) throw new TaskNotFoundException();
        task = taskService.changeTaskStatusByName(
                value, readTaskStatus()
        );
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

}
