package com.tsc.skuschenko.tm.command.project;

import com.tsc.skuschenko.tm.api.service.IProjectService;
import com.tsc.skuschenko.tm.constant.InformationConst;
import com.tsc.skuschenko.tm.constant.TerminalConst;
import com.tsc.skuschenko.tm.exception.entity.ProjectNotFoundException;
import com.tsc.skuschenko.tm.model.Project;
import com.tsc.skuschenko.tm.util.TerminalUtil;

public class ProjectFinishByIndexCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConst.PROJECT_FINISH_BY_INDEX;
    }

    @Override
    public String description() {
        return "finish project by index";
    }

    @Override
    public void execute() {
        showOperationInfo(TerminalConst.PROJECT_FINISH_BY_INDEX);
        showParameterInfo(InformationConst.INDEX);
        final Integer value = TerminalUtil.nextNumber() - 1;
        final IProjectService projectService
                = serviceLocator.getProjectService();
        final Project project = projectService.completeProjectByIndex(value);
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }

}
