package com.tsc.skuschenko.tm.exception.system;

import com.tsc.skuschenko.tm.constant.TerminalConst;
import com.tsc.skuschenko.tm.exception.AbstractException;

public class UnknownArgumentException extends AbstractException {

    public UnknownArgumentException() {
        super("Error! Argument does not found...");
    }

    public UnknownArgumentException(final String argument) {
        super("Error! Argument '" + argument + "' does not found...");
        final String result = "Pleas print '"
                + TerminalConst.HELP
                + "' for more information";
        System.out.println(result);
    }

}
