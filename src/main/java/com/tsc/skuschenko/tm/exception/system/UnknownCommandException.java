package com.tsc.skuschenko.tm.exception.system;

import com.tsc.skuschenko.tm.constant.TerminalConst;
import com.tsc.skuschenko.tm.exception.AbstractException;

public class UnknownCommandException extends AbstractException {

    public UnknownCommandException() {
        super("Error! Command does not found...");
    }

    public UnknownCommandException(final String command) {
        super("Error! Command '" + command + "' does not found...");
        final String result = "Pleas print '"
                + TerminalConst.HELP
                + "' for more information";
        System.out.println(result);
    }

}
